/* ------------------------- */
/* ------ INFORMATION ------ */
/* Author : 
/* Version : 1.0
/* Date : 
/* Description :
/* ------------------------- */

// Fonction jQuery : exécute le script une fois que le DOM est chargé
$(function() {

	/* ---------- ---------- */
	/* ----- SETTINGS ------ */
	/* ---------- ---------- */

	// Objet littéral qui stocke les informations du joueur
	var	Player = {
			money: 30, // Argent de départ du joueur
			life : 20, // Vie de départ du joueur
			speed: 10, // Vitesse de déplacement des monstres : 1 = rapide; 10 = normal; 20 = lent
			time : 4, // Temps (en secondes) avant le début de la vague
			level: 1, // Niveau du joueur
		}; 

	/* ---------- ---------- */
	/* -------- PATH ------- */
	/* ---------- ---------- */

	// Objet littéral qui stocke le chemin des monstres
	var	Path = {
			start: 600, // Départ du chemin sur l'axe X (en px)
			size: 150, // Largeur du chemin (en px)
			step: [ // Tableau à deux dimensions comprenant les étapes du chemin ['up'/'down'/'left'/'right, {number}]
				['down' ,200],
				['left' ,400],
				['down' ,500],
			]
		};

	createPath(Path);

	/* ---------- ---------- */
	/* ------ TOWERS ------- */
	/* ---------- ---------- */

	var towers = [];  // Tableau qui stocke toutes les tours du jeu

	displayTowers(Player); 

	createTowers(Player, towers);

	/* ---------- ---------- */
	/* ----- MONSTERS ------ */
	/* ---------- ---------- */

	var	monsters = []; // Tableau qui stocke tous les monstres du jeu

	/* ---------- ---------- */
	/* ------- GAME -------- */
	/* ---------- ---------- */

	startGame(Player, Path, monsters, towers);
})

// ------------------------------------------------------------------------- //
// ----------------------- ALL FUNCTIONS FOR THE GAME ---------------------- //
// ------------------------------------------------------------------------- //

// ----------------------
// --- FUNCTIONS GAME ---
// ----------------------

/** 
 * @function startGame - Fonction qui "démarre le jeu" :
 * 		1) Affiche les informations du joueur (argent, temps restant avant la prochaine vague, vies restantes, niveau),
 * 		2) Appelle la fonction qui crée les monstres du jeu,
 * 		3) Lance le décompte avant que les monstres se déplacent et que les tours les attaquent 
 */
function startGame(Player, Path, monsters, towers) {
	// 1) On affiche les informations du joueur dans la page html
	$('.infos span.time').text(Player.time);
	$('.infos span.life').text(Player.life);
	$('.infos span.money').text(Player.money);
	$('.infos span.level').text(Player.level);

	// 2) On crée les monstres du jeu
	createMonsters(Path, monsters); 

	// 3) On lance le décompte
	var timer = setInterval(function() {
		Player.time--; // On décompte le temps du joueur restant avant le début de la vague
		$('.infos span.time').text(Player.time); // ... et on l'affiche dans la page html

		if (Player.time == 0) { // Si le décompte est à 0
			clearInterval(timer); // On arrête le décompte

			monstersMove(Player, Path, monsters, towers);
		}
	}, 1000); // 1000ms = 1s
}

// ----------------------
// -- FUNCTIONS OTHERS --
// ----------------------

/**
 * Fonction qui calcul l'hypotenuse
 */
function calculateHypotenuse(a, b) {
  return(Math.sqrt((a * a) + (b * b)));
}

/**
 * Fonction qui retourne une valeur entière en % d'une valeur par rapport à sa valeur maximale.
 * Elle est utilisée afin de transformer les hp d'un monstre en % pour l'affichage de la barre restante de vie du monstre.
 * Elle est également utilisée afin d'afficher la barre de progression de construction d'une tour en %
 * @example
 * percentageProgressBar(50, 200); // return 25
 */
function percentageProgressBar (val, valMax) {
	return parseInt(val * 100 / valMax);
}